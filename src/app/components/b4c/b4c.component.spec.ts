import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { B4cComponent } from './b4c.component';

describe('B4cComponent', () => {
  let component: B4cComponent;
  let fixture: ComponentFixture<B4cComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ B4cComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(B4cComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
